module.exports = config:
  files:
    javascripts:
      joinTo:
        'js/vendor.js': /^bower_components/
        'js/app.js': /^app/
    stylesheets: joinTo: 'css/app.css'
  plugins:
    autoReload:
      enabled:
        css: on
        js: on
        assets: on
  conventions:
    assets: /(assets|font-awesome)/
