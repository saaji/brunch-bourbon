# Installation

Make sure you have brunch and bower

```
npm install -g brunch
npm install -g bower
```

Within project dir init npm packages and bower dependencies

```
npm install
bower install
```

# Running

To start dev. server run

```
brunch watch --server
```

The dev. server will be accessible via http://localhost:3333/

# Building/releasing

To make static build use

```
brunch build
```

To make production ready release do

```
brunch build --production
```
